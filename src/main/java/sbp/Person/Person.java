package sbp.Person;
import java.util.Objects;

/**
 *    Класс Person{name, city, age}, определить метод toString.
 *    Класс Person реализует интерфейс Comparable<Person>, который обеспечивает следующий порядок:
 *    - Сортировка сначала по полю city, а затем по полю name;
 *    - Поля name, city отличны от null;
 */

public class Person implements Comparable<Person>{

    private String name;
    private String city;
    private Integer age;

    /**
     * Конструктор - создание нового объекта
     * @see Person#Person(String, String, int)
     * @exception NullPointerException
     */
    public Person(String name, String city, int age) {
        if (name == null || city == null) {
            throw new NullPointerException();
        }
        this.name = name;
        this.city = city;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public int getAge() {
        return age;
    }

    /**
     * Метод для определения порядка сравнения обьектов
     * Сравнение по полям
     *
     * @param o - обьект переданный в качестве парамтра.
     * @return возвращает 0, если обьекты равны.
     * @return -1, если вызывающий обьект, меньше обьекта переданного в качестве параметра.
     * @return 1, если вызывающий обьект, больше переданного в качестве параметра.
     */

    @Override
    public int compareTo(Person o) {
        int result = this.city.compareToIgnoreCase(o.city);
        if (result == 0) {
            return this.name.compareToIgnoreCase(o.name);
        } else
            return result;
    }


    /**
     * Метод сравнения обьектов
     * @param o принимаемый параметр
     * @return true, если ссылки на обьекты равны или обьекты равны
     * @return false, если принимаемы обьект null или классы обьектов различны
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age && Objects.equals(name, person.name) && Objects.equals(city, person.city);
    }

    /**
     * Генерирует hashCode
     * @return hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, city, age);
    }


    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", age=" + age +
                "}\n";
    }

}