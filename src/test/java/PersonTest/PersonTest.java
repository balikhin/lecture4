package PersonTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import sbp.Person.Person;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class PersonTest
{

    @Test
    public void testToString()
    {
        List<Person> list = new LinkedList<>();

        list.add(new Person("Дмитрий", "Красноярск", 35));

        Assertions.assertEquals(list.toString().getClass(), String.class);
    }

    @Test
    public void compareToTest()
    {
        List<Person> personList = new ArrayList<>();

        personList.add(new Person("Владимир","Ангарск",12));
        personList.add(new Person("Антон","Москва",22));
        personList.add(new Person("Яна","Якутск",36));
        personList.add(new Person("Евгений","Нижний",45));
        personList.add(new Person("Марат","Нижний",64));

        Person person = new Person("Яна","Якутск",36);

        Assertions.assertEquals(person, personList.get(2));
        personList.sort(Person::compareTo);
        Assertions.assertEquals(person, personList.get(4));

        Assertions.assertThrows(RuntimeException.class,() -> new Person(null, "Нижний", 64));

    }
}
